import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Dashboard from './Components/Dashboard';
import GasPrice from './Components/GasPrice'
import NavBar from './Components/Shared/Navbar';
import {
    BrowserRouter as Router,
    Routes as Switch,
    Route,
    Link
  } from "react-router-dom";

ReactDOM.render(
  <Router basename={"crypto-dashboard"}>
  <div>
    <NavBar />
    <Switch>
      <Route path="/Gas" element={<GasPrice />} />
      <Route path="/Home" element={<App />} />
      <Route path="/test" element={<Test />} />
      <Route path="/" element={<Home />} />
    </Switch>
  </div>
</Router>,
  document.getElementById('root')
);

function Home() {
  return <h2>Home</h2>;
}

function Test() {
  return <h2>Test</h2>;
}